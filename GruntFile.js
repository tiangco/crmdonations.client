﻿module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-ng-constant');
    grunt.initConfig({
        ngconstant: {
            // Options for all targets
            options: {
                space: '  ',
                wrap: '(function () {\n \'use strict\';\n\n {%= __ngModule %} })();',
                name: 'app',
                deps: false
            },
            //BI service
            //var _BASE_API_URL = "https://svc.worldvision.ca/businessintelligenceservice/api/";
            //var _BASE_API_URL = "https://stg-svc.worldvision.ca/businessintelligenceservice/api/";
            // var _BASE_API_URL = "http://stg-svc.worldvision.ca/businessintelligenceservice/api/";
            //var _BASE_API_URL = "http://stg-svc.worldvision.ca/businessintelligenceservice/api/";

            // Environment targets
            local: {
                options: {
                    dest: 'app/appConstants.js'
                },
                constants: {
                    _API_SETTINGS: {
                        _BASE_API_URL: 'https://stg-svc.worldvision.ca/businessintelligenceservice/api/'
                    }
                }
            },
            development: {
                options: {
                    dest: 'app/appConstants.js'
                },
                constants: {
                    _API_SETTINGS: {
                        _BASE_API_URL: 'https://stg-svc.worldvision.ca/businessintelligenceservice/api/'
                    }
                }
            },
            staging: {
                options: {
                    dest: 'app/appConstants.js'
                },
                constants: {
                    _API_SETTINGS: {
                        _BASE_API_URL: 'https://stg-svc.worldvision.ca/businessintelligenceservice/api/',
                    }
                }
            },
            production: {
                options: {
                    dest: 'app/appConstants.js'
                },
                constants: {
                    _API_SETTINGS: {
                        _BASE_API_URL: 'https://svc.worldvision.ca/businessintelligenceservice/api/',
                    }
                }
            }
        }
    });

    grunt.registerTask('dev', function (target) {
        if (target === 'dist') {
            return grunt.task.run([
              'build',
              'open',
              'connect:dist:keepalive'
            ]);
        }

        grunt.task.run([
          'ngconstant:development'
        ]);
    });

    grunt.registerTask('stg', [
      'ngconstant:staging'
    ]);

    grunt.registerTask('prd', [
      'ngconstant:production'
    ]);
    grunt.registerTask('local', [
      'ngconstant:local'
    ]);

};
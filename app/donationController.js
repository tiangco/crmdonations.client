﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('donationController', donationController);

    donationController.$inject = ['$scope', '$window', '$filter', '_API_SETTINGS', 'BIService'];
    function donationController($scope, $window, $filter, _API_SETTINGS, BIService) {
        console.log("donationController loaded");
        var vm = this;
        vm.donorId = 0;
        vm.getUrlParameter = getUrlParameter;
        vm.donorId = vm.getUrlParameter('donorId');
        vm.lang = vm.getUrlParameter('lang');
        vm.order = order;

        // donations
        vm.donationData = [];
        vm.donationData.data = [];
        vm.donationData.numPerPage = 10;
        vm.donationData.currentPage = 1;
        vm.donationData.isLoaded = false;
        vm.donationData.isLoading = false;
        vm.donationData.showPager = false;

        console.log("donorId={0}", vm.donorId);
        console.log("lang={0}", vm.lang);
        if (vm.lang == undefined || vm.lang == '') {
            vm.lang = 'en';
        }
        if (vm.donorId != undefined && vm.donorId != '') {
            var encryptedDonorId = BIService.encrypt(vm.donorId);
            console.log("encryptedDonorId={0}", encryptedDonorId);
            BIService.getDonations(encryptedDonorId, vm.lang)
                                    .then(function (response) {
                                        // success
                                        vm.donationData.data = response.Data;
                                        vm.donationData.showPager = vm.donationData.data && vm.donationData.data.length > vm.donationData.numPerPage;
                                        //vm.order('donationData', 'donationDate', true);
                                    },
                                    function (response) {
                                        // error
                                        console.log("error when calling getDonations", response);
                                    });
        }


        //Donations Pagination
        $scope.$watch('vm.donationData.currentPage', function () {
            vm.donationData.start = ((vm.donationData.currentPage - 1) * vm.donationData.numPerPage);
            vm.donationData.end = vm.donationData.start + vm.donationData.numPerPage;
        });
        $scope.$watch('vm.donationData.numPerPage', function () {
            vm.donationData.start = ((vm.donationData.currentPage - 1) * vm.donationData.numPerPage);
            vm.donationData.end = vm.donationData.start + vm.donationData.numPerPage;
        });

        function getUrlParameter(param, dummyPath) {
            var sPageURL = dummyPath || window.location.search.substring(1),
                sURLVariables = sPageURL.split(/[&||?]/),
                res;

            for (var i = 0; i < sURLVariables.length; i += 1) {
                var paramName = sURLVariables[i],
                    sParameterName = (paramName || '').split('=');

                if (sParameterName[0] === param) {
                    res = sParameterName[1];
                }
            }

            return res;
        }

        function order(table, field, order) {
            var orderBy = $filter('orderBy');
            // Check if we're reordering the current field, switch ordering if we are
            if (vm[table].sortType === field) {
                vm[table].reverse = !vm[table].reverse;
            }
                // Allow forced ordering
            else if (order !== '') {
                vm[table].reverse = order;
            }

            vm[table].sortType = field;
            vm[table].data = orderBy(vm[table].data, field, vm[table].reverse);
            vm[table].currentPage = 1;
        }
    }
})();
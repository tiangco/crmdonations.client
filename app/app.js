﻿(function () {
    'use strict';

    angular.module('app', ['ui.bootstrap']);

    angular.module('app').factory('helperService', helperService);

    helperService.$inject = ['$q', '$http'];
    function helperService($q, $http) {

        return {
            httpGetJson: httpGetJson,
            httpPatchJson: httpPatchJson,
            httpPostJson: httpPostJson,
            httpDeleteJson: httpDeleteJson,
            parseErrors: parseErrors
        };

        function httpGetJson(url) {
            var deffered = $q.defer();
            var config = {
                headers: { 'Accept': 'application/json' }
            };

            $http.get(url, config)
                .success(deffered.resolve)
                .error(deffered.reject);

            return deffered.promise;
        }

        function httpPostJson(url, body) {
            var deffered = $q.defer();
            var config = {
                headers: { 'Accept': 'application/json' }
            };

            $http.post(url, body, config)
                .success(deffered.resolve)
                .error(function (data, status) {
                    deffered.reject(data, status);
                });
            return deffered.promise;
        }

        function httpDeleteJson(url, body) {
            var deffered = $q.defer();
            var config = {
                headers: { 'Accept': 'application/json' }
            };

            $http.delete(url, body, config)
                .success(deffered.resolve)
                .error(function (data, status) {
                    deffered.reject(data, status);
                });
            return deffered.promise;
        }

        function httpPatchJson(url, body) {
            var deffered = $q.defer();
            var config = {
                headers: { 'Content-type': 'application/json' }
            };

            $http.patch(url, body, config)
                .success(deffered.resolve)
                .error(function (data, status) {
                    deffered.reject(data, status);
                });


            return deffered.promise;
        }

        function parseErrors(errorResponse) {
            var errors = [];

            if (errorResponse) {
                if (typeof errorResponse.message !== "undefined") {
                    errors.push(errorResponse.message);
                }

                for (var key in errorResponse.modelState) {
                    for (var i = 0; i < errorResponse.modelState[key].length; i++) {
                        errors.push(errorResponse.modelState[key][i]);
                    }
                }
            }

            return errors;
        }
    }
})();
﻿/// <reference path="BIService.js" />
(function () {
    'use strict';

    angular
        .module('app')
        .factory('BIService', BIService);

    BIService.$inject = ['$http', '$q', '_API_SETTINGS', 'helperService'];

    function BIService($http, $q, _API_SETTINGS, helperService) {

        var _BASE_API_URL = _API_SETTINGS._BASE_API_URL;
        var _RSA_KEY = "<RSAKeyValue><Modulus>69QYJDQWH8KvwvXuvs7UoHBLSt/vjP/ZQQZanmn2+vIo1lH9dq86kMn6OjoJBnN7ybDr7RAI0lxrEiOnHvHjxCsSsJE99cRNBWIY2WPHrt0vBZq+K6bfUgmSsoFupuJ/G6ENLSBvXlV8Es1j538ZXYTqekQTz/SwHYPOa6ISzZ/8/tql1ycuWeuN/NTD+Nv99SvmkgTQ+La+pH93Go62dQUQLE2ohNCMJr+Jkr5JeP1F8R4ObSr3hCgurZSERD/KqNKhRSiPTB766/QnQW4DPbjDSbps2IDpVO9NqSr4lUvUmtS72WS43pFSSSsnt6ungnvHYvZE/ZML90ewj/In2w==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
        var PRIVATE_KEY_TEXT = "<RSAKeyValue><Modulus>69QYJDQWH8KvwvXuvs7UoHBLSt/vjP/ZQQZanmn2+vIo1lH9dq86kMn6OjoJBnN7ybDr7RAI0lxrEiOnHvHjxCsSsJE99cRNBWIY2WPHrt0vBZq+K6bfUgmSsoFupuJ/G6ENLSBvXlV8Es1j538ZXYTqekQTz/SwHYPOa6ISzZ/8/tql1ycuWeuN/NTD+Nv99SvmkgTQ+La+pH93Go62dQUQLE2ohNCMJr+Jkr5JeP1F8R4ObSr3hCgurZSERD/KqNKhRSiPTB766/QnQW4DPbjDSbps2IDpVO9NqSr4lUvUmtS72WS43pFSSSsnt6ungnvHYvZE/ZML90ewj/In2w==</Modulus><Exponent>AQAB</Exponent><P>+wPR/tr+BfA7ITgOwQxmdaoq6f9OnT8etOFGUgpZhhdD/ZxsYdiBbeiXKenKjKw7O8Sj94JYJrjsP7LxT8rZc0M9xtVPb9n2GKWKpzq6OZ/uh39oQ/dkzK5G/bw2AX6xmwU83E0o//lV4u5jxr2AJ0ohsaR7qc2BCXOLq5PhpAE=</P><Q>8IMQo8SjibfSVNvTL31Q2+mJTAs4YocS3y50KbjPXcJrz+NI/FgFTU/McCMlWizsnEnrEMPqSbD4gyeNpoPv3947zKpVEjPf6XyU3Q33w6b7/ySNVPwpbdjgSSKtP1eBpurusUGwiIscLhFzhta1GqOv+BKfKqH5bbOhK86e29s=</Q><DP>7Yhq8tHG6lWvjrrec15JovUZ0P7xJKpzY1V+VupGVzay905L3bekmx4r1dTQAJiHIQLu6qwkwNfjcE/kPM7HQWSTFBINLtpCIIaek6tmSFuIvB4by51TfME6mqe9L2L1rK2jLxGxR7Fpzeoq7wmqOQPUqKbvAvteLVRzCjVRpAE=</DP><DQ>klfdlxh+Pb0JETNU0++XdMgsD4ZAP8tak0xLyFs4ah9zxGHStiDE0R7+ETmBb9Yn/o0HqALdmwtAm2VEzo43NeueTVFTRCkmVIbGeZ8XJAiGCuBt0slAWCN/jWC5M/KF5E/M/zQuO1cRxYI+3kvklJoG55ZFHQbDo5mKNwKZLMM=</DQ><InverseQ>msuDI/cyou0CwmcR5Q64ejD3x7c8pj6YV7LDeXe+CmCQHIQ8Lh+XPdZ4kHsyb2gT0HMWQ04ETa4P0tpDzACIDEeIA4eD7V3wTxSE8qWRFdSwiUVkU7tEVlFqxRZ23YdLufHx4jZQqFsQ+LCTr1jw/RrBIKYNfaBrJbJiRtUBwFo=</InverseQ><D>vEd5+rWHEFZcAvhcYwQ4Z9sfkajV1SmS50JOaPlK0TEy/2E4TmA6DszitUgOWpMqum1A/uR1VpWoejPywlb2tHB7HHLpF+VgZLLcCfuKugYm+39+4tT4qMOx1khV8nEUcBeVsHib0aHITC6k23by5mIu3eyqMLY3QWkxDWe5QWY4piJhVxucBn+9cSMlQS/zXGG1k4cU1RPSWjImRLGrgo6Ai9GOTlscrqDBy39p6Bf7Ag2UKM4uxv4bAMzNqLKrXWjxAMeb1yVlU6YrcxCSZmhjrSlGzxweCXbwTOETR0M0HYKyzZrwwJMBiayjlPp6wUVMKSAqbZgcf0VgLNL4AQ==</D></RSAKeyValue>";

        return {
            getDonations: getDonations,
            encrypt: encrypt,
            decrypt: decrypt
        };

        function getDonations(donorId, lang) {
            console.log("calling BIService.getDonations, donorId=", donorId);
            var url = _BASE_API_URL + "alldonations?donorId=" + donorId+"&lang="+lang;
            return helperService.httpGetJson(url);
        }

        function encrypt(plain) {
            var rsa = new System.Security.Cryptography.RSACryptoServiceProvider();
            rsa.FromXmlString(_RSA_KEY);

            var decryptedBytes = System.Text.Encoding.UTF8.GetBytes(plain);
            // Create a new instance of RSACryptoServiceProvider.

            // Encrypt byte array.
            var encryptedBytes = rsa.Encrypt(decryptedBytes, true);
            // Convert bytes to base64 string.
            var encryptedString = System.Convert.ToBase64String(encryptedBytes);
            return encryptedString;
        }

        function decrypt(encrypted) {

            var encryptedBytes = System.Convert.FromBase64String(encrypted);

            // Decrypt text
            var rsa = new System.Security.Cryptography.RSACryptoServiceProvider();
            rsa.FromXmlString(PRIVATE_KEY_TEXT);

            var plainBytes = rsa.Decrypt(encryptedBytes, true);

            // Write decrypted text
            return System.Text.Encoding.UTF8.GetString(plainBytes);
        }
    }
})();